package main

import "fmt"
import "strings"
import "strconv"

func main() {
	refString := "10/ 2"
	setArab := "I II III IV V VI VII VIII IX X"
	setRim := "1 2 3 4 5 6 7 8 9 10"
	opOneTest := "IX"
	opTwoTest := "2"
	fmt.Println(strings.Contains(setArab, opOneTest))
	fmt.Println(strings.Contains(setRim, opTwoTest))

	strng := "42"
	i, _ := strconv.Atoi(strng)
	fmt.Println(i)

	//otseivaem znaki
	signsFilter := func(r rune) bool {
		return strings.ContainsRune("+-*/, ", r)
	}

	//otseivaem tsifry
	lettersFilter := func(r rune) bool {
		return strings.ContainsRune("0123456789IXV ", r)
	}

	operatorS := strings.FieldsFunc(refString, signsFilter)
	fmt.Println(operatorS)
	opOne, _ := strconv.Atoi(operatorS[0])
	opTwo, _ := strconv.Atoi(operatorS[1])
	//for idx, operator := range operatorS {
	//	fmt.Printf("Word %d is: %s\n", idx, operator)
	//}

	expressionS := strings.FieldsFunc(refString, lettersFilter)
	expression := expressionS[0]
	fmt.Println(expression)

	switch expression {
	case "+":
		fmt.Print(opOne + opTwo)
	case "-":
		fmt.Print(opOne - opTwo)
	case "*":
		fmt.Print(opOne * opTwo)
	case "/":
		fmt.Print(opOne / opTwo)

	}
}

//sdelat' capslokom vse simvoly
//proverka kolichestva operandov
//proverka deistviya
//proverit prinadlezhnost pervogo operanda k naboru isArab ili isRim (ot 1 do 10)
//proverit prinadlezhnost vtorogo operanda k naboru isArab ili isRim (ot 1 do 10)
//proverka logikoy vhozdenie operandov v odnu gruppu
//proverit opOneType = opTwoType , esli ne ravno - vyvesty isklyuchenie o neravenstve arab i rim, zatem stop
//if arab togda ispolzuem func arabToRim, myCalc, rimToArab
//if rim togda myCalc
